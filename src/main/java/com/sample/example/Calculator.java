package com.sample.example;

public class Calculator {

	private static int numberOfOccurances(String source, char character) {
		int count = 1;
		for(int i =0; i< source.length(); i++){
			if(source.charAt(i) == character){
				count++;
			}
		}
		return count;
	}

	public static String getRepeatedCharacterCount(String source) {
		String characterRepetation = "";
		
		for (int i = 0; i < source.length(); i++) {
			
			if(characterRepetation.contains(""+ source.charAt(i))){
				continue;
			}
			
			characterRepetation += source.charAt(i) + "" + numberOfOccurances(source, source.charAt(i));
		}
		return characterRepetation;
	}
}
