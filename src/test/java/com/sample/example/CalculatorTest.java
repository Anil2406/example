package com.sample.example;


import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class CalculatorTest {
  @Test
  public void characterRepeat() {
	  assertEquals(Calculator.getRepeatedCharacterCount("AAABB"), "A3B2");
  }
  
  @Test
  public void characterCaseRepeat() {
	  assertEquals(Calculator.getRepeatedCharacterCount("AAABB"), "A3B2");
  }
}
